package com.sbs.internetbanking.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "USERS", schema = "bot_bank")
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	private String username;
	@NotNull(message="Please select a password")
	//@Length(min=8, max=15, message="Password should be between 8 - 15 charactes")
	private String password;
	
	
	//@NotNull
	private String confirmPassword;

//	@AssertTrue(message="passVerify field should be equal than pass field")
//	  private boolean isValid() {
//	    return this.password.equals(this.confirmPassword);
//	  }
	private Date lastLogin;

	@Transient
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	//private int role;
	private int attempts;
	private String accountStatus;
	private String question1;
	private String answer1;
	private String question2;
	private String answer2;
	private UserProfileInfo userProfileInfo;
	private Set<Role> roles = new HashSet<Role>();

	@ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="USER_TO_ROLE", 
                joinColumns={@JoinColumn(name="USERNAME")}, 
                inverseJoinColumns={@JoinColumn(name="ROLE_ID")})
    public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Id
	@Column(name = "USERNAME")
	public String getUsername() {
		return username;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "USERNAME")
	public UserProfileInfo getUserProfileInfo() {
		return userProfileInfo;
	}

	public void setUserProfileInfo(UserProfileInfo userProfileInfo) {
		this.userProfileInfo = userProfileInfo;
	}

	@Column(name = "PASSWORD")
	public String getPassword() {
		return password;
	}

	@Column(name = "LAST_LOGIN")
	public Date getLastLogin() {
		return lastLogin;
	}

	@Column(name = "LOGIN_ATTEMPTS")
	public int getAttempts() {
		return attempts;
	}

	@Column(name = "ACCOUNT_STATUS")
	public String getAccountStatus() {
		return accountStatus;
	}

	@Column(name = "SEC_QUESTION1")
	@JoinColumn(name = "QID")
	public String getQuestion1() {
		return question1;
	}

	@Column(name = "ANSWER1")
	public String getAnswer1() {
		return answer1;
	}

	@Column(name = "SEC_QUESTION2")
	@JoinColumn(name = "QID")
	public String getQuestion2() {
		return question2;
	}

	@Column(name = "ANSWER2")
	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

}