package com.sbs.internetbanking.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "TRANSACTIONS", schema = "bot_bank")
public class Transaction implements Serializable {
    private static final long serialVersionUID = 1L;
    private String transactionId;
    private String transactionType;
    private String transactionFromAccountNum;
    private String transactionToAccountNum;
    private String transactionState;
    private Timestamp tranUpdateTS;

    @Id
    @Column(name = "TRANSACTION_ID")
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Column(name = "TRANSACTION_TYPE")
    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    @Column(name = "TRANSACTION_FROM_USER")
    public String getTransactionFromAccountNum() {
        return transactionFromAccountNum;
    }

    public void setTransactionFromAccountNum(String transactionFromAccountNum) {
        this.transactionFromAccountNum = transactionFromAccountNum;
    }

    @Column(name = "TRANSACTION_TO_USER")
    public String getTransactionToAccountNum() {
        return transactionToAccountNum;
    }

    public void setTransactionToAccountNum(String transactionToAccountNum) {
        this.transactionToAccountNum = transactionToAccountNum;
    }

    @Column(name = "STATE")
    public String getTransactionState() {
        return transactionState;
    }

    public void setTransactionState(String transactionState) {
        this.transactionState = transactionState;
    }

    @Column(name = "TRANSACTION_UPDATE_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getTranUpdateTS() {
        return tranUpdateTS;
    }

    public void setTranUpdateTS(Timestamp tranUpdateTS) {
        this.tranUpdateTS = tranUpdateTS;
    }
}
