package com.sbs.internetbanking.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sbs.internetbanking.enums.AccountStatus;
import com.sbs.internetbanking.persistence.UserManager;

@Service("userDetailsService")
public class SBSUserDetailsService implements UserDetailsService {

	@Autowired
	UserManager userManager;

	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		com.sbs.internetbanking.model.User user = userManager
				.getUserByUserName(username);
//		List<GrantedAuthority> authorities = prepareUserAuthorityList(user
//				.getRoles().iterator().next().getRoleId());
		List<GrantedAuthority> authorities = prepareUserAuthorityList();
		System.out.println(authorities);
		return prepareUserForAuthentication(user, authorities);
	}

	private User prepareUserForAuthentication(
			com.sbs.internetbanking.model.User user,
			List<GrantedAuthority> authorities) {
		if (user.getAccountStatus().equals(AccountStatus.LOCKED.name())){
			return new User(user.getUsername(), user.getPassword(), true, true,
					true, false, authorities);
		}
		else
			return new User(user.getUsername(), user.getPassword(), true, true,
					true, true, authorities);
	}

	private List<GrantedAuthority> prepareUserAuthorityList() {
		//Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		//setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		String roleValue = "";
//		if(role == 0){
//			roleValue = "ROLE_USER";
//		}
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		//authList.add(new SimpleGrantedAuthority(roleValue));
		authList.add(new SimpleGrantedAuthority("ROLE_USER"));
		return authList;
	}
}