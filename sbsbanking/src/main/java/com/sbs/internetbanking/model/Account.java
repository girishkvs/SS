package com.sbs.internetbanking.model;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNT" , schema = "sbs_bank")
public class Account implements Serializable{
	private static final long serialVersionUID = 1L;

	private String accountNumber;
	private String userName;
	private String accountType;
	private Date lastUpdateTS;
	private String accountStatus;
	private Date accountCreationTS;
	@Id
	@Column(name="ACCOUNTNUM")
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	@Column(name="USERNAME")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column(name="ACCOUNT_TYPE")
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	@Column(name="LASTUPDATE")
	public Date getLastUpdateTS() {
		return lastUpdateTS;
	}
	public void setLastUpdateTS(Date lastUpdateTS) {
		this.lastUpdateTS = lastUpdateTS;
	}
	@Column(name="ACCOUNT_STATUS")
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	@Column(name="ACCOUNT_CREATION_TIMESTAMP")
	public Date getAccountCreationTS() {
		return accountCreationTS;
	}
	public void setAccountCreationTS(Date accountCreationTS) {
		this.accountCreationTS = accountCreationTS;
	}

	
	
	
}
