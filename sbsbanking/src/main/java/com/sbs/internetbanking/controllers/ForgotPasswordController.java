package com.sbs.internetbanking.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.sbs.internetbanking.model.User;
import com.sbs.internetbanking.persistence.UserManager;
import com.sbs.internetbanking.service.OTPService;

@Controller
public class ForgotPasswordController {

	private static final Logger logger = LoggerFactory.getLogger(ForgotPasswordController.class);

	@Autowired
	UserManager userManager;

	@Autowired
	MessageSource messageSource;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	OTPService otpService;

	@RequestMapping(value = "/getUserSecQuestions", method = RequestMethod.GET)
	public @ResponseBody String getSecurityQuestions(HttpServletRequest request, @RequestParam(name = "username") String username) {
		Gson gson = new Gson();
		User user = userManager.getUserByUserName(username);
		if (user != null) {
			request.getSession().setAttribute("user", user);
			return gson.toJson(userManager.getUserSecurityQuestions(user));
		}
		return null;
	}

	@RequestMapping(value = "/getSecurityAnswer", method = RequestMethod.GET)
	public @ResponseBody String getSecurityAnswer(HttpServletRequest request, @RequestParam(name = "username") String username,
			@RequestParam(name = "securityQuestion") String securityQuestion,
			@RequestParam(name = "securityAnswer") String securityAnswer) {
		Gson gson = new Gson();
		User user = (User) request.getSession().getAttribute("user");
		Map<String, String> securityQA = userManager.getUserSecurityAnswer(user, securityQuestion, securityAnswer);
		if (securityQA.containsKey(securityQuestion)) {
			if (passwordEncoder.matches(securityAnswer, securityQA.get(securityQuestion)))
				return gson.toJson(true);
		}
		return null;
	}

	@RequestMapping(value = "/getOTP", method = RequestMethod.GET)
	public String getOTP(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		User user = (User) request.getSession().getAttribute("user");
		otpService.sendOTPViaMail(user);
		model.setViewName("welcome");
		return "forgotpassword";
	}
}