package com.sbs.internetbanking.utilities;

import com.sbs.internetbanking.enums.RequestType;
import com.sbs.internetbanking.model.Request;
import com.sbs.internetbanking.model.User;

public class RequestGenerator {
	public static Request newAccountApprovalRequest(User user) {
		Request newAccountApprovalRequest = new Request();
		newAccountApprovalRequest.setRequestFromAccountNum(user.getUsername());
		newAccountApprovalRequest.setRequestId(IdentifierGenerator.getRequestIdForNewAccountApproval(user));
		newAccountApprovalRequest.setRequestType(RequestType.ACCOUNT_APPROVAL.toString());
		newAccountApprovalRequest.setRequestToAccountNum("ANONYMOUS");
		newAccountApprovalRequest.setRequestContent("Approve new request");
		// newAccountApprovalRequest.setCreationTimeStamp();
		return newAccountApprovalRequest;
	}
}