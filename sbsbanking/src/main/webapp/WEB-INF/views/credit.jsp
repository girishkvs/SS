<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ page session="false" %>
<html>
<head>
    <title>Credit Funds</title>
</head>
<body>
<jsp:include page="header.jsp"/>
<h1>Credit</h1>

<form action="#" method="post" id="creditForm">
    Account Number: &nbsp;&nbsp;&nbsp;<input type="text" name="${accountNumber}"/><br/>
    Amount: &nbsp;&nbsp;&nbsp;<input type="text" name="${amount}"/><br/>
    Date: &nbsp;&nbsp;&nbsp;<input type="text" name="${transferDate}"/><br/>
    Amount in the account:"${finalAmount}"
</form>
<jsp:include page="footer.jsp"/>

</body>
</html>