package com.sbs.internetbanking.springconfig;

import com.sbs.internetbanking.service.BankingFunctionsService;
import com.sbs.internetbanking.service.BankingFunctionsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfig {

    @Bean
    public BankingFunctionsService bankingFunctionsService(){
        return new BankingFunctionsServiceImpl();
    }
}