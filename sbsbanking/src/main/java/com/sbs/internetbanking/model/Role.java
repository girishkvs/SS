package com.sbs.internetbanking.model;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name = "ROLES" , schema = "bot_bank")
public class Role implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String roleId;
	private int viewFlag;
	private int deleteFlag;
	private int modifyFlag;
	private int createFlag;
	private Set<User> users = new HashSet<User>();
	
	@ManyToMany(mappedBy = "roles")
    public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	@Id  
	@Column(name="ROLE_ID")
	public String getRoleId() {
		return roleId;
	}
	@Column(name="VIEW_ACCESS")
	public int getViewFlag() {
		return viewFlag;
	}
	public void setViewFlag(int viewFlag) {
		this.viewFlag = viewFlag;
	}
	@Column(name="DELETE_ACCESS")
	public int getDeleteFlag() {
		return deleteFlag;
	}
	public void setDeleteFlag(int deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	@Column(name="MODIFY_ACCESS")
	public int getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(int modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	@Column(name="CREATE_ACCESS")
	public int getCreateFlag() {
		return createFlag;
	}
	public void setCreateFlag(int createFlag) {
		this.createFlag = createFlag;
	}
	
}
