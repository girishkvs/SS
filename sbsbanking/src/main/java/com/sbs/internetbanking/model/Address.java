package com.sbs.internetbanking.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ADDRESS", schema = "bot_bank")
public class Address implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String addressLine1;
	private String addressLine2;
	private String addressId;
	private String city;
	private String areaCode;
	private String state;

	@Column(name="ADDRESS_LINE1")
	public String getAddressLine1() {
		return addressLine1;
	}

	@Column(name="ADDRESS_LINE2")
	public String getAddressLine2() {
		return addressLine2;
	}

	@Id
	@Column(name="ADDRESS_ID")
	public String getAddressId() {
		return addressId;
	}

	@Column(name="CITY")
	public String getCity() {
		return city;
	}

	@Column(name="AREA_CODE")
	public String getAreaCode() {
		return areaCode;
	}

	@Column(name="STATE_ID") 
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

}
