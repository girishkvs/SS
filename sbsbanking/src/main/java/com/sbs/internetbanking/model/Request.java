package com.sbs.internetbanking.model;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "REQUESTS" , schema = "bot_bank")
public class Request implements Serializable{
	private static final long serialVersionUID = 1L;
	private String requestId;
	private String requestType;
	private String requestFromAccountNum;
	private String requestToAccountNum;
	private Timestamp creationTimeStamp;
	private Timestamp approvedTimeStamp;
	private String requestContent;
	private String requestStatus;

	@Id
	@Column(name="REQ_ID")
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	@Column(name="REQ_TYPE")
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	@Column(name="REQ_FROM_USER")
	public String getRequestFromAccountNum() {
		return requestFromAccountNum;
	}
	public void setRequestFromAccountNum(String requestFromAccountNum) {
		this.requestFromAccountNum = requestFromAccountNum;
	}
	@Column(name="REQ_TO_USER")
	public String getRequestToAccountNum() {
		return requestToAccountNum;
	}
	public void setRequestToAccountNum(String requestToAccountNum) {
		this.requestToAccountNum = requestToAccountNum;
	}
	@Column(name="CREATION_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreationTimeStamp() {
		return new Date();
	}
	public void setCreationTimeStamp(Timestamp creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	@Column(name="APPROVE_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getApprovedTimeStamp() {
		return new Date();
	}
	public void setApprovedTimeStamp(Timestamp approvedTimeStamp) {
		this.approvedTimeStamp = approvedTimeStamp;
	}
	@Column(name="REQ_CONTENT")
	public String getRequestContent() {
		return requestContent;
	}
	public void setRequestContent(String requestContent)
	{
		this.requestContent = requestContent;
	}
	@Column(name = "REQ_STATUS")
	public String getRequestStatus() {
		return requestStatus;
	}

	@Column
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
}
