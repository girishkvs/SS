package com.sbs.internetbanking.persistence;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sbs.internetbanking.model.Account;

public class AccountManager {

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public void saveAccount(Account request) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(request);
	}

	@Transactional
	public void deleteAccount(Account request) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(request);
	}

	@Transactional
	public void updateAccount(Account request) {
		Session session = sessionFactory.getCurrentSession();
		session.update(request);
	}
	
	@Transactional
	public List getAccounts(String userName){
		Session session = sessionFactory.getCurrentSession();
		List accList=session.createQuery("FROM account where username="+userName).list();
		return accList;
	}
}