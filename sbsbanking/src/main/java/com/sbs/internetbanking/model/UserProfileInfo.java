package com.sbs.internetbanking.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "USERPII", schema = "bot_bank")
public class UserProfileInfo implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Size(min=8, max=15, message="Your username should be between 8 - 15 characters.")
	@NotNull(message="Please select a username")
	private String username;
	//@Size(min=8, max=15, message="Your username should be between 8 - 15 characters.")
	@NotNull(message="Please select a username")
	private String firstname;
	@Size(min=8, max=15, message="Your username should be between 8 - 15 characters.")
	@NotNull(message="Please select a username")
	private String lastname;
	//@Pattern(regexp="(^$|[0-9]{10})")
	@NotNull(message="Please enter your 10 digit phone number")
	private String phone;
	@NotNull(message="Please provide date of birth")
	private String dob;
	@NotNull(message="enter your emailId")
	private String emailId;
	//@Size(min=9, max =9 , message="enter a valid 9 digit SSN")
	@NotNull(message="enter your SSN")
	private String ssn;
	private String idProof;
	private String idType;
	private Address address;
	
	//@Column(name="dob")
	@Transient
	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}
	
	@Id
	@Column(name="USERNAME")
	public String getUsername() {
		return username;
	}
	
	@Column(name="FIRSTNAME")
	public String getFirstname() {
		return firstname;
	}
	
	@Column(name="LASTNAME")
	public String getLastname() {
		return lastname;
	}
	
	@Column(name="PHONE")
	public String getPhone() {
		return phone;
	}
	
	@Column(name="EMAILID")
	public String getEmailId() {
		return emailId;
	}
	
	@Column(name="SSN")
	public String getSsn() {
		return ssn;
	}
	
	@Column(name="ID_PROOF")
	public String getIdProof() {
		return idProof;
	}
	
	@Column(name="ID_TYPE")
	public String getIdType() {
		return idType;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "ADDRESS_ID")
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public void setIdProof(String idProof) {
		this.idProof = idProof;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
