package com.sbs.internetbanking.persistence;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.sbs.internetbanking.enums.RequestType;
import com.sbs.internetbanking.model.Request;
import com.sbs.internetbanking.model.User;

public class RequestManager {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	UserManager userManager;

	@Transactional
	public void saveRequest(Request request) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(request);
	}

	@Transactional
	public void deleteRequest(Request request) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(request);
	}

	@Transactional
	public void updateRequest(Request request) {
		Session session = sessionFactory.getCurrentSession();
		session.update(request);
	}

	@Transactional
	public List getRequestList(String userName) {
		Session session = sessionFactory.getCurrentSession();
		List reqList = session.createQuery("FROM Request where username=" + userName).list();
		return reqList;
	}

	// @Transactional
	public void declineRequest(Request requestId, RequestType reqType) {

	}

	// @Transactional
	public void approveRequest(Request requestId, RequestType reqType) {
		switch (reqType) {
		case ACCOUNT_APPROVAL:
			approveAccount(requestId);
			break;
		case DEBIT_REQUEST:
			break;
		case CREDIT_REQUEST:
			break;
		}
	}

	@Transactional
	private void approveAccount(Request requestId) {
		Session session = sessionFactory.getCurrentSession();
		Request request = session.get(Request.class, requestId);
		Gson gson = new Gson();
		User usr = gson.fromJson(request.getRequestContent(), User.class);
		userManager.saveUser(usr);
		request.setRequestType(RequestType.APPROVED.toString());
	}
}
