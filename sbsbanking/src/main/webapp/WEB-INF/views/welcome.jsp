<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<html>
<!DOCTYPE html>
<html lang="en">
<head>
<style type="text/css">
.help-inline
{
color: #FF0000;
}
</style>
	
<meta charset="utf-8">


    <script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/welcome.js"></script>
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/gt_favicon.png">
	
 	<link rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700"> 
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">


</head>

<body>
	<!-- Fixed navbar -->
    <jsp:include page="header.jsp"></jsp:include>
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">

		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li class="active">User access</li>
		</ol>

		<div class="row">
			
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<header class="page-header">
				
					<h1 class="page-title">Welcome</h1>
					<c:url value="/logout" var="logoutUrl" />
					<form action="${logoutUrl}" method="post" id="logoutForm" class="form-horizontal">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					
					<c:if test="${pageContext.request.userPrincipal.name != null}">
						<h2>
							User : ${pageContext.request.userPrincipal.name} | <a
								href="javascript:formSubmit()"> Logout</a>
						</h2>
					</c:if>
					</form>
				</header>
				<sec:authorize access="hasRole('ROLE_USER')">
		 
		
		
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
						<!-- 	<h3 class="thin text-center">Sign in to your account</h3> -->
							<p></p>
							<hr>
							<form name='choiceForm' method='POST' class="form-horizontal" id="choiceForm" action="<c:url value='/login' />">


									<div class="form-group">
										<label for="choice" class="col-lg-4 control-label" style="margin-left: 0%; text-align: left;">How
											would you like to receive temporary code?</label>
										<div class="col-lg-6">
										<label for="email" class="col-lg-8 control-label">Email</label><input type="radio" value="email" name="email" id="email"
												class="col-lg-3" /> <br/>
												<label for="textOnPhone" class="col-lg-8 control-label" >Text on Phone</label><input type="radio"
												value="textOnPhn" name="textOnPhn" id="textOnPhn"
												class="col-lg-3" /> 
												<label
												class="help-inline" id="noChoiceError"></label>

										</div>

									</div>
									<div class="row">
									<div class="col-lg-4 text-right">
								
										<input name="btnGenerateOTP" type="button" value="Generate OTP" class="btn btn-action"/> 
									</div>
										
									</div>
                              </form>
                              	<form name='verificationForm' method='GET' class="form-horizontal" id="choiceForm" action="<c:url value='/verifyOTPandPassword' />">
                              
								<!-- 	<div class="form-group" id="otpDiv" style="visibility: hidden;"> -->
									<div class="form-group" id="otpDiv">
										<label for="otp" class="col-lg-4 control-label" style="margin-left: 0%; text-align: left;">Verification
											Code</label>
										<div class="col-lg-8">
											<input type="text" class="form-control" id="verificationCode"
												name="verificationCode"> <label class="help-inline"
												id="verificationCodeError"></label>
										</div>

									</div>
									<!-- <div class="form-group" id="passwordDiv" style="visibility: hidden;"> -->
										<div class="form-group" id="passwordDiv">
										<label for="otp" class="col-lg-4 control-label" style="margin-left: 0%; text-align: left;">Password</label>
										<div class="col-lg-8">
											<input type="text" class="form-control" id="verificationCode"
												name="verificationCode"> <label class="help-inline"
												id="verificationCodeError"></label>
										</div>

									</div>
									<div class="row">
									<div class="col-lg-4 text-right">
								
										<input name="btnGenerateOTP" type="submit" value="Continue" class="btn btn-action"/> 
									</div>
										
									
										
									</div>

									<hr>


								
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
							</form>
						</div>
					</div>

				</div>
				
				
				</sec:authorize>
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->
	

	 <jsp:include page="footer.jsp"></jsp:include>
		




	
</body>
</html>
</html>