package com.sbs.internetbanking.controllers;

import com.sbs.internetbanking.service.BankingFunctionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller 
public class BankingFunctionsController {

	@Autowired
	private BankingFunctionsService bankingFunctionsService;

	@RequestMapping(value="/creditfunds",method=RequestMethod.POST)
	public ModelAndView creditFundsPage(@RequestParam String accountNumber, @RequestParam String amount, @RequestParam String transferDate){
		ModelAndView creditModel=new ModelAndView();
		creditModel.setViewName("credit");
		int finalAmount = creditFunds(Integer.parseInt(accountNumber), Integer.parseInt(amount));
		creditModel.addObject(finalAmount);
		return creditModel;
	}

	//TODO: WIP
	private int creditFunds(int accountNumber, int amount){
		return 0;
	}
	@RequestMapping(value="/debitfunds",method=RequestMethod.POST)
	public ModelAndView debitFundsPage(@RequestParam String accountNumber,@RequestParam String amount)
	{
		ModelAndView debitModel=new ModelAndView();
		debitModel.setViewName("debit");
		double finalAmount=debitFunds(Integer.parseInt(accountNumber),Integer.parseInt(amount));
		debitModel.addObject(finalAmount);
		return debitModel;
			
	}
	private double debitFunds(int accountNumber,int amount)
	{
		return (Double) null;
	}
	@RequestMapping(value="/transfer",method=RequestMethod.POST)
	public ModelAndView transferFundsPage(@RequestParam String accountNumber,@RequestParam String toAccountNumber,@RequestParam String amount,@RequestParam String date)
	{
		ModelAndView transferModel=new ModelAndView();
		transferModel.setViewName("transfer");
		double finalAmount=debitFunds(Integer.parseInt(accountNumber),Integer.parseInt(amount));
		transferModel.addObject(finalAmount);
		return transferModel;
			
	}
	private double transferFunds(double accountNumber, double amount)
	{
		return (Double) null;
	}
}