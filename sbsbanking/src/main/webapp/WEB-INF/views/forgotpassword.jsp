<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page session="true"%>
<html>
<!DOCTYPE html>
<html lang="en">
<head>
<style type="text/css">
.help-inline {
	color: #FF0000;
}
</style>
<script type="text/javascript">
	
</script>

<meta charset="utf-8">


<script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/forgotPassword.js"></script>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/gt_favicon.png">

<link rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">

<!-- Custom styles for our template -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">

<script type="text/javascript">
	$(document).ready(function() {
		$("#error").css("visibility", "hidden");
		$("#answer1_Error").css("visibility", "hidden");
		$("#answer2_Error").css("visibility", "hidden");
		$("#question1").css("visibility", "hidden");
		$("#question2").css("visibility", "hidden");

	});
	$(document).ready(function() {
		$("#username").click(function() {
			$("#error").css("visibility", "hidden");
			$("#answer1_Error").css("visibility", "hidden");
			$("#answer2_Error").css("visibility", "hidden");
			$("#question1").css("visibility", "hidden");
			$("#question2").css("visibility", "hidden");
			$("#generateOTP").css("visibility", "hidden");
			$("#verifyUser").css("visibility", "visible");

		});
	});

	$(document).ready(function() {
		$("#verifyUsername").click(function() {
			$.ajax({
				url : "${pageContext.request.contextPath}/getUserSecQuestions",
				type : "GET",
				data : {
					username : $("#username").val()
				},
				dataType : "json",
				success : function(questions) {
					$("#question1").css("visibility", "visible");
					$("#question2").css("visibility", "visible");
					$("#generateOTP").css("visibility", "visible");
					$("#verifyUser").css("visibility", "hidden");
					var obj = 1;
					$.each(questions, function(key, value) {
						$("#q" + obj).text(value.qText);
						obj = 2;
					});
				},
				error : function(data) {

					$("#error").css("visibility", "visible");
				}
			});
		});
	});

	$(document).ready(function() {
		$("#answer1").blur(function() {
			$.ajax({
				url : "${pageContext.request.contextPath}/getSecurityAnswer",
				type : "GET",

				data : {
					username : $("#username").val(),
					securityQuestion : $("#q1").text(),
					securityAnswer : $("#answer1").val()
				},
				dataType : "json",
				success : function(questions) {
					$("#answer1_Error").css("visibility", "hidden");
				},
				error : function(data) {
					$("#answer1_Error").css("visibility", "visible");
				}
			});
		});
	});

	$(document).ready(function() {

		$("#answer2").blur(function() {

			$.ajax({
				url : "${pageContext.request.contextPath}/getSecurityAnswer",
				type : "GET",
				data : {
					username : $("#username").val(),
					securityQuestion : $("#q2").text(),
					securityAnswer : $("#answer2").val()
				},
				dataType : "json",
				success : function(questions) {
					$("#answer2_Error").css("visibility", "hidden");
				},
				error : function(data) {
					$("#answer2_Error").css("visibility", "visible");
				}
			});
		});
	});
	
	$(document).ready(function() {
		$("#generateOTP").click(function() {
			$.ajax({
				url : "${pageContext.request.contextPath}/getOTP",
				type : "GET",
				success : function(questions) {
					$("#answer2_Error").css("visibility", "hidden");
				},
				error : function(data) {
					$("#answer2_Error").css("visibility", "visible");
				}
			});
		});
	});
	
</script>

</head>

<body>
	<!-- Fixed navbar -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">

		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li class="active">Forgot Password</li>
		</ol>

		<div class="row">

			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<header class="page-header">
					<h1 class="page-title">Forgot Password</h1>
				</header>

				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<!-- 	<h3 class="thin text-center">Sign in to your account</h3> -->
							<p></p>
							<hr>
							<form name='forgotPassword'>



								<div class="form-group">
									<label for="username" class="col-lg-12 control-label"><spring:message code="message.enterusername"
											text="Enter username"></spring:message></label>
									<div class="col-lg-8">
										<input type="text" class="form-control" id="username" name="username" placeholder="User Name" onblur="usernameCheck()" />
										<label class="help-inline" id="usernameError"></label>
										<div id="error" style="visibility: hidden;" class="col-lg-10">
											<label for="username_not_found" class="help-inline"><spring:message code="message.usernotfound"></spring:message></label>
										</div>
									</div>
								</div>

								<div class="form-group" id="question1" style="visibility: hidden;">
									<label id="q1" class="col-lg-12 control-label"></label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="answer1" placeholder="Answer" onchange="answer1Check()" /> <label
											class="help-inline" id="answer1Error"></label>
										<div id="answer1_Error" style="visibility: hidden;" class="col-lg-10">
											<label for="answer_not_verified" class="help-inline"><spring:message code="message.answerincorrect"></spring:message></label>
										</div>
									</div>
								</div>

								<div class="form-group" id="question2" style="visibility: hidden;">
									<label id="q2" class="col-lg-12 control-label"></label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="answer2" placeholder="Answer" onchange="answer2Check()" /> <label
											class="help-inline" id="answer2Error"></label>
										<div id="answer2_Error" style="visibility: hidden;" class="col-lg-10">
											<label for="answer_not_verified" class="help-inline"><spring:message code="message.answerincorrect"></spring:message></label>
										</div>
									</div>
								</div>
								<hr>

								<div class="row" style="text-align: center">
									<div class="col-lg-4 text-right" id="verifyUser">
										<input type="button" id="verifyUsername" name="verify" value="verify" class="btn btn-action" />
									</div>

									<div class="col-lg-4 text-right" style="visibility: hidden;" id="generateOTP">
										<input type="button" id="generateOTP" name="generateOTP" value="GET OTP" class="btn btn-action" />
									</div>
								</div>
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
							</form>
						</div>
					</div>

				</div>

			</article>
			<!-- /Article -->

		</div>
	</div>
	<!-- /container -->


	<jsp:include page="footer.jsp"></jsp:include>






</body>
</html>
</html>