package com.sbs.internetbanking.controllers;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sbs.internetbanking.model.User;
import com.sbs.internetbanking.persistence.ContentManager;
import com.sbs.internetbanking.persistence.UserManager;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	UserManager userManager;
	
	@Autowired
	ContentManager contentManager;

	@Autowired
	MessageSource messageSource;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		return "welcome";
	}
	@RequestMapping(value = {"/verifyOTPandPassword" }, method = RequestMethod.GET)
	public String verifyOTPandPassword(Locale locale, Model model) {
		return "dashboard";
	}
	
	@RequestMapping(value = { "/about"}, method = RequestMethod.GET)
	public String about(Locale locale, Model model) {
		System.out.println("about");
		return "about";
	}
	
	@RequestMapping(value = { "/contactUs"}, method = RequestMethod.GET)
	public String contactUs(Locale locale, Model model) {
		return "contactus";
	}

	@RequestMapping(value = "/myaccount", method = RequestMethod.GET)
	public String myAccount(Locale locale, Model model) {
		model.addAttribute("welcome", messageSource.getMessage("message.welcome", null, Locale.US));
		return "home";
	}
	
	@RequestMapping(value = { "/getSecurityQuestions"}, method = RequestMethod.GET)
	public @ResponseBody String getSecurityQuestions() {
		Gson gson = new Gson();
		return(gson.toJson(contentManager.getSecurityQuestions()).toString());
	}	
	
	@RequestMapping(value = { "/getStates"}, method = RequestMethod.GET)
	public @ResponseBody String getStates() {
		Gson gson = new Gson();
		return(gson.toJson(contentManager.getStates()).toString());
	}	
	@RequestMapping(value = { "/getUser"}, method = RequestMethod.GET)
	public @ResponseBody String getUser(@RequestParam(name = "username") String username) {
		Gson gson = new Gson();
		System.out.println("HELLO");
		System.out.println(username);
		User user = userManager.getUserByUserName(username);
		if (user == null) {
			
			return gson.toJson(true);
		}
		return null;
	}
	
	@RequestMapping(value = { "/getEmail"}, method = RequestMethod.GET)
	public @ResponseBody String getEmail(@RequestParam(name = "emailid") String emailid) {
		Gson gson = new Gson();
		
		List userpiiList = userManager.getUserByEmail(emailid);
		if (userpiiList == null || userpiiList.size()==0) {
			
			return gson.toJson(true);
		}
		
		return null;
	}
}