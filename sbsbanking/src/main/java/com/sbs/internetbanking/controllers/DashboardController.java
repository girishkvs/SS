package com.sbs.internetbanking.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.internal.util.compare.EqualsHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sbs.internetbanking.model.User;
import com.sbs.internetbanking.persistence.RequestManager;
import com.sbs.internetbanking.persistence.UserManager;
@Controller
public class DashboardController {
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	UserManager userManager;

	@Autowired
	MessageSource messageSource;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	RequestManager requestManager;
	
	@RequestMapping(value = "/getProfile", method = RequestMethod.GET)
	public ModelAndView getProfile(HttpServletRequest request) {
		Principal principal = request.getUserPrincipal();
		System.out.println(principal.getName());
		ModelAndView model = new ModelAndView();
		User user=userManager.getUserByUserName(principal.getName());
		System.out.println("date"+user.getUserProfileInfo().getDob());
		System.out.println("address"+user.getUserProfileInfo());

		model.addObject("user", user);
		model.setViewName("viewProfile");
		return model;
	}
	@RequestMapping(value = "/getUpdateProfile", method = RequestMethod.GET)
	public ModelAndView getUpdateProfile(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Principal principal = request.getUserPrincipal();
		User user=userManager.getUserByUserName(principal.getName());
		System.out.println(user.getUserProfileInfo().getSsn());
		model.addObject("user", user);
		model.setViewName("updateProfile");
		return model;
	}
	
	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	public ModelAndView updateProfile(@ModelAttribute("user") User user,HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		System.out.println("date"+user.getUserProfileInfo().getDob());
		System.out.println("address"+user.getUserProfileInfo());
		System.out.println(user.getUserProfileInfo().getSsn());
		//userManager.updateUser(user);
		model.addObject("user", user);
		model.setViewName("viewProfile");
		return model;
	}
}
