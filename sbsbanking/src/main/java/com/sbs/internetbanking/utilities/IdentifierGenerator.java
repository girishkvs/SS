package com.sbs.internetbanking.utilities;

import com.sbs.internetbanking.model.Address;
import com.sbs.internetbanking.model.User;

public class IdentifierGenerator {

	public static String getAddressId(Address address){
		return address.getCity()+ address.getState()+"10001";
	}
	
	public static String getRequestIdForNewAccountApproval(User user){
		return "NAA"+ user.getUsername().substring(0, 5);
	}
	
}
