package com.sbs.internetbanking.service;

import java.util.Date;
import java.util.Properties;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sbs.internetbanking.model.User;

/**
 * 
 * @author
 *
 */
public class OTPService {

	private static final Logger logger = LoggerFactory.getLogger(OTPService.class);

	private final static Properties properties = new Properties();
	private static String bankEmailAddress;
	private static String otpText;

	public OTPService(String host, String auth, String debug, String port, String bankEmailAddress, String otpText) {
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.auth", auth);
		properties.put("mail.debug", debug);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.socketFactory.fallback", "false");
		properties.put("mail.smtp.starttls.enable", "true");
		this.bankEmailAddress = bankEmailAddress;
		this.otpText = otpText;
	}

	public static String truncate(String value, int length) {
		if (value != null && value.length() > length)
			value = value.substring(0, length);
		return value;
	}

	public static void sendMail(String mailid, long otp) {
		try {
			Session mailSession = Session.getInstance(properties, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(bankEmailAddress, "SecureBankingSystem16");
				}
			});

			mailSession.setDebug(true);
			Message msg = new MimeMessage(mailSession);
			msg.setFrom(new InternetAddress(bankEmailAddress));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailid));
			msg.setSentDate(new Date());
			msg.setSubject(otpText);
			String txt = String.valueOf(otp);
			msg.setText(txt);
			Transport.send(msg);

		} catch (Exception exp) {
			logger.debug("An Error Occurred", exp);
		}
	}

	public void sendOTPViaMail(User user) {
		try {
			String key = String.valueOf(System.currentTimeMillis());
			String message = user.getUsername() + user.getUserProfileInfo().getPhone().toString();
			String truncated_result;

			long mask = 0x7FFFFFFF;
			long res = 0;
			long int_value = 0;

			Mac sha256_HMAC = Mac.getInstance("HmacSHA1");
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA1");
			sha256_HMAC.init(secret_key);
			byte[] rawHmac = sha256_HMAC.doFinal(message.getBytes());
			truncated_result = truncate(Hex.encodeHexString(rawHmac), 4);
			int_value = Long.parseLong(truncated_result, 16);
			res = int_value & mask;
			double mod = res % (Math.pow(10, 4));
			long otp = (long) mod;
			sendMail(user.getUserProfileInfo().getEmailId(), otp);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}