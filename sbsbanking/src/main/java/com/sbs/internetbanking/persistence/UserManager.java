package com.sbs.internetbanking.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sbs.internetbanking.enums.AccountStatus;
import com.sbs.internetbanking.model.SecurityQuestion;
import com.sbs.internetbanking.model.User;

public class UserManager {

	@Autowired
	SessionFactory sessionFactory;
	
	private int allowedAttempts = 0;

	public UserManager(int allowedAttempts){
		this.allowedAttempts = allowedAttempts;
	}
	
	@Transactional
	public void saveUser(User user) {
		
		Session session = sessionFactory.getCurrentSession();
		session.persist(user);
	}

	@Transactional
	public void deleteUser(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(user);
	}

	@Transactional
	public void updateUser(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.update(user);
	}
	

	@Transactional
	public User getUserByUserName(String userName) {
		Session session = sessionFactory.getCurrentSession();
		try{
			User usr = session.get(User.class, userName);
			System.out.println(usr);
			return usr;	
		}catch(HibernateException he){
			return null;
		}
		
	}

	@Transactional
	public void updateLoginAttempts(String userName) {
		Session session = sessionFactory.getCurrentSession();
		User usr = session.get(User.class, userName);
		System.out.println(usr.toString());
		int attempts = usr.getAttempts();
		if (attempts < allowedAttempts - 1) {
			usr.setAttempts(attempts + 1);
			session.update("LOGIN_ATTEMPTS", usr);
		} else {
			usr.setAccountStatus(AccountStatus.LOCKED.toString());
		}
	}

	@Transactional
	public void resetLoginAttempts(String userName) {
		Session session = sessionFactory.getCurrentSession();
		User user = session.get(User.class, userName);
		user.setAttempts(0);
		session.update("LOGIN_ATTEMPTS", user);
	}

	@Transactional
	public List<SecurityQuestion> getUserSecurityQuestions(User user) {
		List<SecurityQuestion> questions = new ArrayList<SecurityQuestion>();
		Session session = sessionFactory.getCurrentSession();
		questions.add(session.get(SecurityQuestion.class, user.getQuestion1()));
		questions.add(session.get(SecurityQuestion.class, user.getQuestion2()));
		return questions;
	}
	@Transactional
	public List getUserByEmail(String emailid) {
		Session session = sessionFactory.getCurrentSession();
		
		try{
			Query query = session.createQuery("from UserProfileInfo where EMAILID = :id ");
			query.setParameter("id", emailid);
			List list = query.list();

			return list;
		}catch(HibernateException he){
			System.out.println(he);
			return null;
		}

	}
	@Transactional
	public Map<String, String> getUserSecurityAnswer(User user,String securityQuestion,String securityAnswer) {
		
		Map<String, String>  securityQA =new HashMap<String, String>();
		Session session = sessionFactory.getCurrentSession();
		SecurityQuestion q1=session.get(SecurityQuestion.class, user.getQuestion1());
		String answer1=user.getAnswer1();
		securityQA.put(q1.getqText(), answer1);
		SecurityQuestion q2=session.get(SecurityQuestion.class, user.getQuestion2());
		String answer2=user.getAnswer2();
		securityQA.put(q2.getqText(), answer2);
		return securityQA;
	}
}
