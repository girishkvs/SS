package com.sbs.internetbanking.service;

import com.sbs.internetbanking.enums.RequestType;
import com.sbs.internetbanking.model.Request;
import com.sbs.internetbanking.model.Transaction;
import com.sbs.internetbanking.persistence.RequestManager;
import com.sbs.internetbanking.persistence.TransactionsManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.time.Instant;

public class BankingFunctionsServiceImpl implements BankingFunctionsService {

    private static final String STATUS = "Pending Approval";

    @Autowired
    private RequestManager requestManager;
    @Autowired
    private TransactionsManager transactionsManager;

    @Override
    public void creditFunds(int accountNumber, double amount, Instant timestamp) {
        String accountDetails = accountDetails(accountNumber);

        StringBuilder instant = new StringBuilder(timestamp.toString());
        String transactionId = instant.insert(0, "t").toString();
        String requestId = instant.insert(0, "r").toString();

        Request request = new Request();
        request.setRequestId(requestId);
        request.setCreationTimeStamp(Timestamp.from(timestamp));
        request.setRequestFromAccountNum(Integer.toString(accountNumber));
        request.setRequestType(RequestType.CREDIT_REQUEST.name());
        request.setApprovedTimeStamp(null);
        request.setRequestToAccountNum(null);
        request.setRequestStatus(STATUS);
        request.setRequestContent(accountDetails);
        requestManager.saveRequest(request);

        Transaction transaction = new Transaction();
        transaction.setTransactionId(transactionId);
        transaction.setTransactionType(RequestType.CREDIT_REQUEST.name());
        transaction.setTranUpdateTS(Timestamp.from(timestamp));
        transaction.setTransactionFromAccountNum(Integer.toString(accountNumber));
        transaction.setTransactionToAccountNum(null);
        transaction.setTransactionState(STATUS);
        transactionsManager.saveTransaction(transaction);
    }

    @Override
    public void debitFunds(int accountNumber, double amount, Instant timestamp) {
        String accountDetails = accountDetails(accountNumber);

        StringBuilder instant = new StringBuilder(Instant.now().toString());
        String transactionId = instant.insert(0, "t").toString();
        String requestId = instant.insert(0, "r").toString();

        Request request = new Request();
        request.setRequestId(requestId);
        request.setCreationTimeStamp(Timestamp.from(timestamp));
        request.setRequestFromAccountNum(Integer.toString(accountNumber));
        request.setRequestType(RequestType.DEBIT_REQUEST.name());
        request.setApprovedTimeStamp(null);
        request.setRequestToAccountNum(null);
        request.setRequestStatus(STATUS);
        request.setRequestContent(accountDetails);
        requestManager.saveRequest(request);

        Transaction transaction = new Transaction();
        transaction.setTransactionId(transactionId);
        transaction.setTransactionType(RequestType.DEBIT_REQUEST.name());
        transaction.setTranUpdateTS(Timestamp.from(timestamp));
        transaction.setTransactionFromAccountNum(Integer.toString(accountNumber));
        transaction.setTransactionToAccountNum(null);
        transaction.setTransactionState(STATUS);
        transactionsManager.saveTransaction(transaction);
    }

    @Override
    public void transferFunds(int fromAccountNumber, int toAccountNumber, double amount, Instant timestamp) {
        String accountDetails = accountDetails(fromAccountNumber);

        StringBuilder instant = new StringBuilder(Instant.now().toString());
        String transactionId = instant.insert(0, "t").toString();
        String requestId = instant.insert(0, "r").toString();

        Request request = new Request();
        request.setRequestId(requestId);
        request.setCreationTimeStamp(Timestamp.from(timestamp));
        request.setRequestFromAccountNum(Integer.toString(fromAccountNumber));
        request.setRequestToAccountNum(Integer.toString(toAccountNumber));
        request.setRequestType(RequestType.TRANSFER_REQUEST.name());
        request.setApprovedTimeStamp(null);
        request.setRequestStatus(STATUS);
        request.setRequestContent(accountDetails);
        requestManager.saveRequest(request);

        Transaction transaction = new Transaction();
        transaction.setTransactionId(transactionId);
        transaction.setTransactionType(RequestType.TRANSFER_REQUEST.name());
        transaction.setTranUpdateTS(Timestamp.from(timestamp));
        transaction.setTransactionFromAccountNum(Integer.toString(fromAccountNumber));
        transaction.setTransactionToAccountNum(Integer.toString(toAccountNumber));
        transaction.setTransactionState(STATUS);
        transactionsManager.saveTransaction(transaction);
    }

    //TODO: Need to include the logic for getting account details based on account num
    private String accountDetails(int accountNumber){
        return "";
    }
}
