package com.sbs.internetbanking.persistence;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sbs.internetbanking.model.Transaction;

public class TransactionsManager {

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public void saveTransaction(Transaction request) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(request);
	}

	@Transactional
	public void deleteTransaction(Transaction request) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(request);
	}

	@Transactional
	public void updateTransaction(Transaction request) {
		Session session = sessionFactory.getCurrentSession();
		session.update(request);
	}
	
	@Transactional
	public List getTransactions(String accountNumber){
		Session session = sessionFactory.getCurrentSession();
		List tranList=session.createQuery("FROM Transactions where ACCOUNTNUM="+accountNumber).list();
		return tranList;
	}
}