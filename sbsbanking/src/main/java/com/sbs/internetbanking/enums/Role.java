package com.sbs.internetbanking.enums;

public enum Role {

	CUSTOMER, MERCHANT, MANAGER, ADMIN, EMPLOYEE 
	
}
